# Binaries for CosmosApps

## Contents
- [Candle](#candle)
- [Stealth](#stealth)

## Candle
Real-time stock market watcher.

[Changelog](https://gitlab.com/cosmosapps/candle/-/blob/master/CHANGELOG.md)

[0.2.3](Candle/Candle_0.2.3.apk) | [0.2.2](Candle/Candle_0.2.2.apk) | [0.2.1](Candle/Candle_0.2.1.apk) | [0.2.0](Candle/Candle_0.2.0.apk) | [0.1.5](Candle/Candle_0.1.5.apk) | [0.1.4](Candle/Candle_0.1.4.apk) | [0.1.3](Candle/Candle_0.1.3.apk) | [0.1.2](Candle/Candle_0.1.2.apk) | [0.1.1](Candle/Candle_0.1.1.apk) | [0.1.0](Candle/Candle_0.1.0.apk)

## Stealth
Account-free, privacy-oriented, and feature-rich Reddit client.

[Changelog](https://gitlab.com/cosmosapps/stealth/-/blob/master/CHANGELOG.md)

[3.0.0-alpha02](Stealth/Stealth_3.0.0-alpha02.apk) | [3.0.0-alpha01](Stealth/Stealth_3.0.0-alpha01.apk) | [2.3.0](Stealth/Stealth_2.3.0.apk) | [2.2.2](Stealth/Stealth_2.2.2.apk) | [2.2.1](Stealth/Stealth_2.2.1.apk) | [2.2.0](Stealth/Stealth_2.2.0.apk) | [2.1.1](Stealth/Stealth_2.1.1.apk) | [2.1.0](Stealth/Stealth_2.1.0.apk) | [2.0.3](Stealth/Stealth_2.0.3.apk) | [2.0.2](Stealth/Stealth_2.0.2.apk) | [2.0.1](Stealth/Stealth_2.0.1.apk) | [2.0.0](Stealth/Stealth_2.0.0.apk) | [2.0.0-alpha04](Stealth/Stealth_2.0.0-alpha04.apk) | [2.0.0-alpha03](Stealth/Stealth_2.0.0-alpha03.apk) | [2.0.0-alpha02](Stealth/Stealth_2.0.0-alpha02.apk) | [2.0.0-alpha01](Stealth/Stealth_2.0.0-alpha01.apk) | [1.1.3](Stealth/Stealth_1.1.3.apk) | [1.1.2](Stealth/Stealth_1.1.2.apk) | [1.1.1](Stealth/Stealth_1.1.1.apk) | [1.1.0](Stealth/Stealth_1.1.0.apk) | [1.0.1](Stealth/Stealth_1.0.1.apk) | [1.0.0](Stealth/Stealth_1.0.0.apk) | [0.1.1](Stealth/Stealth_0.1.1.apk) | [0.1.0](Stealth/Stealth_0.1.0.apk)
